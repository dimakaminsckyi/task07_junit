package com.epam.minesweeper.model.plateau;

public class PlateauUtil {

    public int countRepeatNumberInArray(int[] array, int index) {
        int count = 1;
        for (int i = index; i < array.length - 1; i++) {
            if (index == array.length) {
                return count;
            }
            if (array[i] == array[i + 1]) {
                count++;
            } else {
                break;
            }
        }
        return count;
    }

    public boolean isPlateau(int[] array, int index) {
        int start = array[index - 1];
        int end = 0;
        int number = array[index];
        int count = countRepeatNumberInArray(array, index);
        if (index + count < array.length) {
            end = array[index + count];
        }
        return number > start && number > end;
    }
}

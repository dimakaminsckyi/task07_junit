package com.epam.minesweeper.model.plateau;

public class Plateau {

    private PlateauUtil util;

    public Plateau() {
        util = new PlateauUtil();
    }

    public int[] returnLongestPlateau(int[] array) {
        int size = 0;
        int startIndex = 0;
        int endIndex = 0;
        for (int i = 1; i < array.length - 1; i++) {
            int currentSize = util.countRepeatNumberInArray(array, i);
            if (util.isPlateau(array, i) && currentSize > size) {
                size = currentSize;
                startIndex = i;
                endIndex = currentSize + i - 1;
            }
        }
        int[] finalArray = new int[size];
        for (int index = startIndex, i = 0; index < endIndex + 1; index++, i++) {
             finalArray[i] = array[index];
        }
        return finalArray;
    }
}

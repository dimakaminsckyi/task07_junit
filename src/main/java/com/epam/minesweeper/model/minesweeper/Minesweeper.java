package com.epam.minesweeper.model.minesweeper;

public class Minesweeper {

    private int column;
    private int row;
    private boolean[][] boolArray;

    public Minesweeper(int m, int n, int p) {
        column = m;
        row = n;
        boolArray = MinesweeperUtil.generateRandomTable(column, row, p);
    }

    public void showMinesweeperTable() {
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                if (boolArray[i][j]) {
                    System.out.print("* ");
                } else {
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }

    public void showNieghboringBombsNumber() {
        int[][] number =
                MinesweeperUtil.generateNeighboringBombsTable(column, row, boolArray);
        for (int i = 0;i < column;i++){
            for (int j = 0; j < row;j++){
                if (boolArray[i][j]){
                    System.out.print("* ");
                }else{
                    System.out.print(number[i][j] + " ");
                }
            }
            System.out.println();
        }
    }


}

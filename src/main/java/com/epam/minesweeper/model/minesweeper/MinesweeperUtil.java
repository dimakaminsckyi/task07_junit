package com.epam.minesweeper.model.minesweeper;

import java.util.Random;

public class MinesweeperUtil {

    public static boolean[][] generateRandomTable(int column, int row, int probability) {
        Random random = new Random();
        boolean[][] table = new boolean[column + 2][row + 2];
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                table[i][j] = random.nextInt(probability) == 0;
            }
        }
        return table;
    }

    public static int[][] generateNeighboringBombsTable(int column, int row, boolean[][] table) {
        int[][] bombsArray = new int[column + 2][row + 2];
        for (int i = 1; i <= column;i++){
            for (int j = 1; j <= row;j++){
                for (int k = i - 1; k <= i + 1; k++)
                    for (int d = j - 1; d <= j + 1; d++)
                        if (table[k][d]){
                            bombsArray[i][j]++;
                        }
            }
        }
        return bombsArray;
    }
}

package com.epam.minesweeper.view;

import com.epam.minesweeper.controller.MinesweeperController;
import com.epam.minesweeper.controller.PlateauController;
import com.epam.minesweeper.controller.impl.MinesweeperControllerImpl;
import com.epam.minesweeper.controller.impl.PlateauControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MainView {

    private static Logger log = LogManager.getLogger(MainView.class);
    private MinesweeperController minesweeperController;
    private PlateauController plateauController;
    private Map<String, String> menu;
    private Map<String, Runnable> methodRun;

    public MainView() {
        initMinesweeper();
        initMenu();
        plateauController = new PlateauControllerImpl();
        methodRun = new LinkedHashMap<>();
        methodRun.put("show", () -> minesweeperController.getMinesweeperTable());
        methodRun.put("bombs", () -> minesweeperController.getNeighboringBombs());
        methodRun.put("plateau" , () -> plateauController.showLongestPlanetau());
        methodRun.put("exit", () -> log.info("GB"));
    }

    public void show() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder flagExit = new StringBuilder("");
        do {
            showMenu();
            try {
                flagExit.setLength(0);
                flagExit.append(scanner.nextLine());
                methodRun.get(flagExit.toString()).run();
            } catch (NullPointerException e) {
                log.warn("Input correct option");
            }
        }while (!flagExit.toString().equals("exit"));
        scanner.close();
    }

    private void initMenu(){
        menu = new LinkedHashMap<>();
        menu.put("show", "show - Show minesweeper table");
        menu.put("bombs", "bombs - Show numbers of neighboring bombs");
        menu.put("plateau" , "plateau - Show largest plateau in array");
        menu.put("exit", "exit - Exit");
    }

    private void initMinesweeper() {
        Scanner scanner = new Scanner(System.in);
        log.info("Initialize minesweeper table.....");
        log.info("Input Column size : ");
        int M = scanner.nextInt();
        log.info("Input row size : ");
        int N = scanner.nextInt();
        log.info("Input probability : ");
        int probability = scanner.nextInt();
        minesweeperController = new MinesweeperControllerImpl(M, N, probability);
    }

    private void showMenu() {
        menu.values().forEach(s -> log.info(s));
    }
}

package com.epam.minesweeper;

import com.epam.minesweeper.view.MainView;

public class App {

    public static void main(String[] args) {
        new MainView().show();
    }

}

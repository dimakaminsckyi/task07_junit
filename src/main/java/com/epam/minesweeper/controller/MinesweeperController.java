package com.epam.minesweeper.controller;

public interface MinesweeperController {

    void getMinesweeperTable();

    void getNeighboringBombs();

}

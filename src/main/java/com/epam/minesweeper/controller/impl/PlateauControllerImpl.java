package com.epam.minesweeper.controller.impl;

import com.epam.minesweeper.controller.PlateauController;
import com.epam.minesweeper.model.plateau.Plateau;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class PlateauControllerImpl implements PlateauController {

    private static Logger log = LogManager.getLogger(PlateauControllerImpl.class);
    private Plateau plateau;

    public PlateauControllerImpl() {
        plateau = new Plateau();
    }

    @Override
    public void showLongestPlanetau() {
        int[] array = plateau.returnLongestPlateau(initPlateauArray());
        for (int a : array){
            System.out.print(a + " ");
        }
    }

    private int[] initPlateauArray(){
        Scanner scanner = new Scanner(System.in);
        log.info("Input size of array : ");
        int size = scanner.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < size;i++){
            log.info("Enter number : ");
            array[i] = scanner.nextInt();
        }
        return array;
    }
}

package com.epam.minesweeper.controller.impl;

import com.epam.minesweeper.controller.MinesweeperController;
import com.epam.minesweeper.model.minesweeper.Minesweeper;

public class MinesweeperControllerImpl implements MinesweeperController {

    private Minesweeper minesweeper;

    public MinesweeperControllerImpl(int M, int N, int p) {
        minesweeper = new Minesweeper(M,N,p);
    }

    @Override
    public void getMinesweeperTable() {
        minesweeper.showMinesweeperTable();
    }

    @Override
    public void getNeighboringBombs() {
        minesweeper.showNieghboringBombsNumber();
    }
}

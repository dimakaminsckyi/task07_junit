package com.epam.model.minesweeper;

import com.epam.minesweeper.model.minesweeper.MinesweeperUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinesweeperUtilTest {

    private static boolean[][] actualBool;
    private static int[][] actualInt;

    @BeforeAll
    static void setUp() {
        actualBool = MinesweeperUtil.generateRandomTable(3, 3, 2);
        actualInt = MinesweeperUtil.generateNeighboringBombsTable(3, 3, actualBool);
    }

    @Test
    void whenGenerateRandomTable_ReturnCorrectColumnSize() {
        int actual = actualBool.length;
        assertEquals(5, actual);
    }

    @Test
    void whenGenerateRandomTable_ReturnCorrectRowSize() {
        int actual = actualBool[0].length;
        assertEquals(5, actual);
    }

    @Test
    void whenGenerateRandomTable_ReturnFalseSize() {
        int actual = actualBool.length;
        assertNotEquals(3, actual);
    }

    @Test
    void whenGenerateRandomTable_ReturnNotNull() {
        assertNotNull(actualBool);
    }

    @Test
    void whenGenerateNeighboringBombsTable_ReturnCorrectSize() {
        int actual = actualInt.length;
        assertEquals(5, actual);
    }

    @Test
    void whenGenerateNeighboringBombsTable_ReturnCorrectArray() {
        int actual = actualInt[0].length;
        assertEquals(5, actual);
    }

    @Test
    void whenGenerateNeighboringBombsTable_ReturnFalse() {
        int actual = actualInt.length;
        assertNotEquals(3, actual);
    }

    @Test
    void whenGenerateNeighboringBombsTable_ReturnNotNull() {
        assertNotNull(actualInt);
    }
}
package com.epam.model.platenau;

import com.epam.minesweeper.model.plateau.PlateauUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlateauUtilTest {

    private static int[] actualArray;
    private static PlateauUtil util;

    @BeforeAll
    static void setUp(){
        util = new PlateauUtil();
        actualArray = new int[]{1,2,2,2,2,5,6,6,6,6,4,5,5,5,5,5};
    }

    @RepeatedTest(3)
    void whenCountRepeatNumberInArray_ReturnCorrectInt() {
        int actual = util.countRepeatNumberInArray(actualArray, 1);
        assertEquals(4, actual);
    }

    @Test
    void whenCountRepeatNumberInArray_ReturnNotNull() {
        Integer actual = util.countRepeatNumberInArray(actualArray, 5);
        assertNotNull(actual);
    }

    @Test
    void whenCountRepeatNumberInArray_ReturnFalse() {
        int actual = util.countRepeatNumberInArray(actualArray, 0);
        assertNotEquals(3, actual);
    }

    @Test
    void whenRunIsPlateau_ReturnCorrectBool() {
        boolean actual = util.isPlateau(actualArray, 6);
        assertTrue(actual);
    }

    @Test
    void givenInvalidData_whenRunIsPlateau_ReturnFalse() {
        boolean actual = util.isPlateau(actualArray, 1);
        assertFalse(actual);
    }

    @Test
    void whenRunIsPlateau_ReturnNotNull() {
        Boolean actual = util.isPlateau(actualArray ,5);
        assertNotNull(actual);
    }

}
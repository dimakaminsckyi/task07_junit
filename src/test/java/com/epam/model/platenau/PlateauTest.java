package com.epam.model.platenau;

import com.epam.minesweeper.model.plateau.Plateau;
import com.epam.minesweeper.model.plateau.PlateauUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class PlateauTest {

    private static int[] actualArray;

    @BeforeAll
    static void setUp(){
        actualArray = new int[]{1,2,2,2,2,2,5,6,6,6,6,4,5,5,5,5,5};
    }

    @InjectMocks
    private Plateau plateau;

    @Mock
    private PlateauUtil plateauUtil;

    @Test
    void whenReturnLongestPlateau_ReturnCorrectArray() {
        when(plateauUtil.countRepeatNumberInArray(actualArray, 1)).thenReturn(5);
        when(plateauUtil.isPlateau(actualArray, 1)).thenReturn(true);
        int[] actual = plateau.returnLongestPlateau(actualArray);
        int[] expected = new int[]{2,2,2,2,2};
        assertArrayEquals(expected , actual);
    }

    @Test
    void whenReturnLongestPlateau_ReturnCorrectSize() {
        when(plateauUtil.countRepeatNumberInArray(actualArray, 1)).thenReturn(5);
        when(plateauUtil.isPlateau(actualArray, 1)).thenReturn(true);
        int actual = plateau.returnLongestPlateau(actualArray).length;
        int expected = new int[]{2,2,2,2,2}.length;
        assertEquals(expected , actual);
    }

    @Test
    void whenReturnLongestPlateau_ReturnFalse() {
        when(plateauUtil.countRepeatNumberInArray(actualArray, 1)).thenReturn(5);
        when(plateauUtil.isPlateau(actualArray, 1)).thenReturn(true);
        int[] actual = plateau.returnLongestPlateau(actualArray);
        int[] expected = new int[]{2,2,2,2,2,2};
        assertFalse(Arrays.equals(expected , actual));
    }

    @Test
    void whenReturnLongestPlateau_ReturnNotNull() {
        when(plateauUtil.countRepeatNumberInArray(actualArray, 1)).thenReturn(5);
        when(plateauUtil.isPlateau(actualArray, 1)).thenReturn(true);
        int[] actual = plateau.returnLongestPlateau(actualArray);
        assertNotNull(actual);
    }

}